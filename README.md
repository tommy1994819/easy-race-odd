# Easy Race Odd #

## API Documentation ##
https://market.mashape.com/hkdatahose/hong-kong-horse-race-data-hose

## Version 1.0 ##

* Show all the odd percentage compare to the last timestamp.
* Allow user input more race and odd.

### What is this repository for? ###

This repository is a simple odd percentage compare with the old timestamp.
The project used angularJs 2.0 and TypeScript to build.

### Dependency ###

This project require nodeJs to deploy. Here is the link: https://nodejs.org/en/

### How do I get set up? ###

Goto your repository folder
```
#!git
cd easy-race-odd/
``` 
Install the needed node dependencies
```
#!git
npm install
npm start
```

### Who do I talk to? ###

TN Tommy (aka the repo owner)