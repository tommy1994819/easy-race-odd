import {ModuleWithProviders} from '@angular/core'
import {Routes,RouterModule} from '@angular/router'

import {HomeComponent} from './components/home.component'
import {RaceComponent} from './components/race.component'
import {AdminComponent} from './components/admin.component'

const appRoutes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'analysis',
		component: RaceComponent
	},
	{
		path: 'admin',
		component: AdminComponent
	}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);