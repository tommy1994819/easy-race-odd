import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { HomeComponent } from './components/home.component';
import { RaceComponent } from './components/race.component';
import { AdminComponent } from './components/admin.component';
import { routing } from './app.routing';

@NgModule({
	imports:      [ BrowserModule , FormsModule, HttpModule, routing ],
	declarations: [ AppComponent , HomeComponent, RaceComponent, AdminComponent ],
	bootstrap:    [ AppComponent ]
})
export class AppModule { }
