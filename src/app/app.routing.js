"use strict";
var router_1 = require('@angular/router');
var home_component_1 = require('./components/home.component');
var race_component_1 = require('./components/race.component');
var admin_component_1 = require('./components/admin.component');
var appRoutes = [
    {
        path: '',
        component: home_component_1.HomeComponent
    },
    {
        path: 'analysis',
        component: race_component_1.RaceComponent
    },
    {
        path: 'admin',
        component: admin_component_1.AdminComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map