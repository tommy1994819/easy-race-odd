import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import 'rxjs/add/operator/map'

@Injectable ()
export class RacesService{
	constructor(private http:Http){
		console.log("RacesService Initialized...");
	}

	getRaces(){
		return this.http.get('app/jsons/race.json')
			.map(res => res.json());
	}
}