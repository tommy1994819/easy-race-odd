"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent() {
        this.name = 'Angular';
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            template: "\n\t<nav class=\"navbar navbar-default\">\n\t<div class=\"container\">\n\t<div class=\"navbar-header\">\n\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n\t<span class=\"sr-only\">Toggle navigation</span>\n\t<span class=\"icon-bar\"></span>\n\t<span class=\"icon-bar\"></span>\n\t<span class=\"icon-bar\"></span>\n\t</button>\n\t<a class=\"navbar-brand\" routerLink=\"/\">Eash Race Odd</a>\n\t</div>\n\n\t<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\t<ul class=\"nav navbar-nav\">\n\t<li><a routerLink=\"/\">Home</a></li>\n\t<li><a routerLink=\"/analysis\">Analysis</a></li>\n\t<li><a routerLink=\"/admin\">Admin</a></li>\n\t</ul>\n\t\n\t</div>\n\t</div>\n\t</nav>\n\n\n\t<div class=\"container\">\n\t<router-outlet></router-outlet>\n\t</div>\n\t",
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map