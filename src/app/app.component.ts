import { Component } from '@angular/core';

@Component({
	selector: 'app',
	template: `
	<nav class="navbar navbar-default">
	<div class="container">
	<div class="navbar-header">
	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" routerLink="/">Eash Race Odd</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	<ul class="nav navbar-nav">
	<li><a routerLink="/">Home</a></li>
	<li><a routerLink="/analysis">Analysis</a></li>
	<li><a routerLink="/admin">Admin</a></li>
	</ul>
	
	</div>
	</div>
	</nav>


	<div class="container">
	<router-outlet></router-outlet>
	</div>
	`,
})
export class AppComponent  { name = 'Angular'; }
