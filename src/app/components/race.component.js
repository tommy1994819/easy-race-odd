"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var races_service_1 = require('../services/races.service');
var RaceComponent = (function () {
    function RaceComponent(racesService) {
        var _this = this;
        this.racesService = racesService;
        this.odd = [];
        this.racesService.getRaces().subscribe(function (races) {
            _this.races = races;
        });
        this.raceForm = {
            id: 0,
            horseForm: {
                time: "",
                odd: []
            }
        };
        this.newRace = true;
        setInterval(function () {
            _this.date = new Date();
        }, 1000);
        this.horseArray = Array(0).fill(0).map(function (x, i) { return i; });
        this.horseNumber = 0;
    }
    RaceComponent.prototype.onRaceNumberChange = function (raceNumber) {
        var i = 0;
        for (var _i = 0, _a = this.races; _i < _a.length; _i++) {
            var race = _a[_i];
            if (race.id == raceNumber) {
                this.newRace = false;
                this.horseNumber = race.horse[0].odd.length;
                this.onHorsesNumberChange(this.horseNumber);
                break;
            }
            i++;
        }
        if (i == this.races.length) {
            this.newRace = true;
            this.horseNumber = 0;
            this.onHorsesNumberChange(this.horseNumber);
        }
    };
    RaceComponent.prototype.onHorsesNumberChange = function (horseNumber) {
        this.horseArray = Array(Number(horseNumber)).fill(0).map(function (x, i) { return i; });
    };
    RaceComponent.prototype.submitRace = function (time) {
        this.raceForm.horseForm.time = time;
        this.raceForm.horseForm.odd = objectToArray(this.odd);
        // TODO:add this race to the json file.
        this.addRace(this.raceForm);
        this.resetRaceForm();
    };
    RaceComponent.prototype.addRace = function (raceForm) {
        if (!this.newRace) {
            for (var _i = 0, _a = this.races; _i < _a.length; _i++) {
                var race_1 = _a[_i];
                if (race_1.id == raceForm.id) {
                    race_1.horse.push(raceForm.horseForm);
                    break;
                }
            }
        }
        else {
            var race = {
                id: -1,
                horse: []
            };
            race.id = raceForm.id;
            race.horse.push(raceForm.horseForm);
            this.races.push(race);
        }
    };
    RaceComponent.prototype.resetRaceForm = function () {
        this.odd = [];
        this.raceForm = {
            id: 0,
            horseForm: {
                time: "",
                odd: []
            }
        };
        this.newRace = true;
        this.horseNumber = 0;
        this.onHorsesNumberChange(this.horseNumber);
    };
    RaceComponent.prototype.caculatePercentage = function (odd1, odd2) {
        return -Math.round(((odd2 - odd1) / odd1) * 100);
    };
    RaceComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'user',
            templateUrl: 'race.component.html',
            providers: [races_service_1.RacesService]
        }), 
        __metadata('design:paramtypes', [races_service_1.RacesService])
    ], RaceComponent);
    return RaceComponent;
}());
exports.RaceComponent = RaceComponent;
function objectToArray(object) {
    var arr = Array(Number());
    for (var i in object) {
        if (object.hasOwnProperty(i)) {
            arr.push(object[i]);
        }
    }
    return arr;
}
//# sourceMappingURL=race.component.js.map