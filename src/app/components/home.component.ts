import {Component} from '@angular/core'

@Component({
	selector: 'user',
	template: `<div class="jumbotron">
	<h1>Easy Race Odd</h1>
	<p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
	<p><a routerLink="/analysis" class="btn btn-primary btn-lg">Learn more</a></p>
	</div>
	`
})

export class HomeComponent {}