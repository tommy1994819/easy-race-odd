import {Component} from '@angular/core'

@Component({
	moduleId: module.id,
	selector: 'admin',
	templateUrl: `admin.component.html`
})
export class AdminComponent { 
	raceForm : raceForm;
	horseNumber : number[];
	date : Date;

	odd:number[] = [];

	constructor(){
		this.raceForm = {
			id : -1,
			horseForm : {
				time : "",
				odd : []
			}
		}

		setInterval(() => {
			this.date = new Date();
		},1000)
		this.horseNumber = Array(1).fill(0).map((x,i)=>i);
	}

	onHorsesNumberChange(horseNumber:number){
		this.horseNumber = Array(Number(horseNumber)).fill(0).map((x,i)=>i);
	}

	addRace(race:number,time:string){
		this.raceForm.id = race;
		this.raceForm.horseForm.time = time;
		this.raceForm.horseForm.odd = this.odd;

		// add this race to the json file.
		console.log(this.raceForm);
	}
}

interface raceForm{
	id: number;
	horseForm: horseForm;
}
interface horseForm{
	time: string;
	odd: number[];
}