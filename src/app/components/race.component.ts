import {Component} from '@angular/core'
import {RacesService} from '../services/races.service'

@Component({
	moduleId: module.id,
	selector: 'user',
	templateUrl: 'race.component.html',
	providers:[RacesService]
})

export class RaceComponent { 
	races : race[];

	raceForm:raceForm;

	newRace : boolean;
	date : Date;
	horseArray : number[];
	horseNumber : number;
	odd:number[] = [];

	constructor(private racesService:RacesService){
		this.racesService.getRaces().subscribe(races => {
			this.races = races;
		})

		this.raceForm = {
			id : 0,
			horseForm : {
				time : "",
				odd : []
			}
		}

		this.newRace = true;

		setInterval(() => {
			this.date = new Date();
		},1000)
		this.horseArray = Array(0).fill(0).map((x,i)=>i);
		this.horseNumber = 0;
	}

	onRaceNumberChange(raceNumber:number){
		var i:number = 0;
		for(let race of this.races){
			if(race.id == raceNumber){
				this.newRace = false;
				this.horseNumber = race.horse[0].odd.length;
				this.onHorsesNumberChange(this.horseNumber);
				break;
			}
			i++;
		}
		if(i == this.races.length) {
			this.newRace = true;
			this.horseNumber = 0;
			this.onHorsesNumberChange(this.horseNumber);
		}
	}

	onHorsesNumberChange(horseNumber:number){
		this.horseArray = Array(Number(horseNumber)).fill(0).map((x,i)=>i);
	}

	submitRace(time:string){
		this.raceForm.horseForm.time = time;
		this.raceForm.horseForm.odd = objectToArray(this.odd);
		// TODO:add this race to the json file.
		this.addRace(this.raceForm);
		this.resetRaceForm();
	}

	addRace(raceForm:raceForm){
		if(!this.newRace){
			for(let race of this.races){
				if(race.id == raceForm.id){
					race.horse.push(raceForm.horseForm);
					break;
				}
			}
		} else {
			var race:race = {
				id:-1,
				horse:[]
			};
			race.id = raceForm.id;
			race.horse.push(raceForm.horseForm);
			this.races.push(race);
		}
	}

	resetRaceForm(){
		this.odd = [];
		this.raceForm = {
			id : 0,
			horseForm : {
				time : "",
				odd : []
			}
		}
		this.newRace = true;
		this.horseNumber = 0;
		this.onHorsesNumberChange(this.horseNumber);
	}

	caculatePercentage(odd1:number,odd2:number){
		return -Math.round(((odd2-odd1)/odd1)*100);
	}
}

// Race Object
interface race{
	id: number;
	horse: horse[];
}
interface horse{
	time: string;
	odd: number[];
}

// Form Object
interface raceForm{
	id: number;
	horseForm: horseForm;
}
interface horseForm{
	time: string;
	odd: number[];
}

function objectToArray(object:any[]){
	var arr = Array(Number());
	for( var i in object ) {
		if (object.hasOwnProperty(i)){
			arr.push(object[i]);
		}
	}
	return arr;
}